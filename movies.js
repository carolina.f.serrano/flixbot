const fetch = require('node-fetch');

const { THE_MOVIE_DB_API_KEY } = process.env;

const getMovieInfo = async (movie) => {
	try {
		const data = await (await fetch( `https://api.themoviedb.org/3/search/movie?api_key=${THE_MOVIE_DB_API_KEY}&language=pt-BR&query=${movie}`)).json();

		if(data.results.length > 0) {
			const details = data.results[0].overview;
			return details;
		} else {
			return 'Não encontrei filmes com esse título!';
		}
	} catch(error) {
		console.log(error);
		throw error;
	}
};

const getPopularMovies = async () => {
	try {
		const data = await(await fetch(`https://api.themoviedb.org/3/movie/popular?api_key=${THE_MOVIE_DB_API_KEY}&language=pt-BR&page=1`)).json();
		const popular = data.results.filter((movie, index) => index <= 4).map(movie => movie.title);
		return popular;
	} catch(error) {
		console.log(error);
		throw error;
	}
}

const getRecommendations = async (movie) => {
	console.log('Movie Query: ', movie);
	try {
		const data = await (await fetch( `https://api.themoviedb.org/3/search/movie?api_key=${THE_MOVIE_DB_API_KEY}&language=pt-BR&query=${movie}`)).json();
		const movieID = data.results[0].id;

		if(movieID) {
			const response = await( await fetch(`https://api.themoviedb.org/3/movie/${movieID}/recommendations?api_key=${THE_MOVIE_DB_API_KEY}&language=pt-BR&page=1`)).json();
			const recommendations = response.results.filter((movie, index) => index <= 4).map(movie => movie.title);
			return recommendations;

		} else {
			return 'Nao consegui achar recomendações para esse filme.';
		}
	}catch(error) {
		console.log(error);
		throw error;
	}
}

const MoviesFunctions = {
	getMovieInfo,
	getPopularMovies,
	getRecommendations
}

module.exports = MoviesFunctions;
