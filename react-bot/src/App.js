import React from 'react';
import Pusher from 'pusher-js';

import './App.css';
import logo from './flixBot.png';

class App extends React.Component {
	constructor(props) {
		super(props);
		this.state = {
			userMessage: '',
			conversation: [],
		};
	}

	componentWillMount() {
		const pusher = new Pusher('8da2d85981c7deae39ba', {
			cluster: 'us2',
			encrypted: true,
		});

		const channel = pusher.subscribe('bot');
		channel.bind('bot-response', data => {
			const msg = {
				text: data.message,
				user: 'ai',
			};

			this.setState({
				conversation: [...this.state.conversation, msg],
			});
		});
	}

	handleChange = event => {
		this.setState({ userMessage: event.target.value });
	};

	handleSubmit = event => {
		event.preventDefault();
		if (!this.state.userMessage.trim()) return;

		const msg = {
			text: this.state.userMessage,
			user: 'human',
		};

		this.setState({
			conversation: [...this.state.conversation, msg],
		});

		fetch('http://localhost:5000/chat', {
			method: 'POST',
			headers: { 'Content-Type': 'application/json' },
			body: JSON.stringify({
				message: this.state.userMessage,
			}),
		});

		this.setState({ userMessage: '' });
	};

	render() {
		const { userMessage, conversation } = this.state;
		const ChatBubble = (text, i, className) => {
			return (
				<div key={`${className}-${i}`} className={`${className} chat-bubble`}>
					<span className="chat-content">{text}</span>
				</div>
			);
		};

		const chat = conversation.map((e, index) =>
			ChatBubble(e.text, index, e.user)
		);

		return (
			<div className="chat-container">
				<div className="chat-header">
					<img src={logo} alt={logo} />
				</div>
				<div className="chat-window">
					<div className="conversation-view">{chat}</div>
					<div className="message-box">
						<form onSubmit={this.handleSubmit} className="chat-form">
							<input
								value={userMessage}
								onInput={this.handleChange}
								className="text-input"
								type="text"
								autoFocus
								placeholder="Digite sua mensagem!"
							/>
							<div className="chat-button-container">
								<button className="chat-button" onClick={this.handleSubmit}>Enviar</button>
							</div>
						</form>
					</div>
				</div>
			</div>
		);
	}
}

export default App;
