# FlixBot

Chatbot integrado com DialogFlow e The Movie DB API, que realiza
consultas sobre sinopses de filmes, recomendações de filmes com base em
um título do usuário e consulta de filmes mais populares  do momento.

Stack:
- Back-end: NodeJS (ao menos versão 10.13.0) e Pusher
- UI: ReactJS

Como rodar localmente:
1) Instalar as dependências do servidor:
  - Na raíz do projeto, execute `npm install`.

2) Instalar as dependências client-side:
  - Entre na pasta "client" e execute `npm install`.

3) Iniciar o servidor
  - Na raiz do projeto, executar o comando `node server.js`

4) Iniciar servidor client-side:
  - Dentro da pasta "react-bot", executar `npm start`

Acessar `localhost:3000` e comecar a conversar com o bot
