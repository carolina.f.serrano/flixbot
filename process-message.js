const Dialogflow = require('dialogflow');
const Pusher = require('pusher');
const Movies = require('./movies');

const projectId = 'weatherbot-fb9be';
const sessionId = 'MESSAGES';
const languageCode = 'pt-BT';

const config = {
	credentials: {
		private_key: process.env.DIALOGFLOW_PRIVATE_KEY,
		client_email: process.env.DIALOGFLOW_CLIENT_EMAIL,
	},
};

const pusher = new Pusher({
	appId: process.env.PUSHER_APP_ID,
	key: process.env.PUSHER_APP_KEY,
	secret: process.env.PUSHER_APP_SECRET,
	cluster: process.env.PUSHER_APP_CLUSTER,
	encrypted: true,
});

const sessionClient = new Dialogflow.SessionsClient(config);

const sessionPath = sessionClient.sessionPath(projectId, sessionId);

const processMessage = (message) => {
	const dialogFlowRequest = {
		session: sessionPath,
		queryInput: {
			text: {
				text: message,
				languageCode,
			},
		},
	};

	sessionClient.detectIntent(dialogFlowRequest)
		.then(responses => {
			const result = responses[0].queryResult;
			const { displayName } = result.intent;

			switch(displayName) {

				case 'detect-movie':
					let movie = result.parameters.fields['movie'].stringValue;

					return Movies.getMovieInfo(movie).then(description => {
						console.log()
						return pusher.trigger('bot', 'bot-response', {
							message: `Aqui está a sinopse do filme que você pediu: \n ${description}`,
						});
					}).catch(error => console.log(error));

				case 'popular-movies':

					return Movies.getPopularMovies().then(movies => {
						return pusher.trigger('bot', 'bot-response', {
							message: `Aqui estão os cinco filmes mais populares do momento: \n${movies}`
						});
					});

				case 'detect-recommendation':
					let query = result.parameters.fields['movie'].stringValue;

					return Movies.getRecommendations(query).then(movies => {
						return pusher.trigger('bot', 'bot-response', {
							message: `Veja aqui algumas recomendações: \n${movies}`
						});
					}).catch(error => console.log(error));

				default:
					console.log('No movie found');
					break;
			}

			return pusher.trigger('bot', 'bot-response', {
				message: result.fulfillmentText,
			});

		}).catch(err => console.error('ERROR:', err));
};

module.exports = processMessage;
